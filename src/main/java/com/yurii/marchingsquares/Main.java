/*
 * Copyright 2015 The "Marching Squares Java" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yurii.marchingsquares;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 26/12/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class Main {

    /**
     * Enumeration of the input source.
     */
    private enum DATA_SOURCE {
        TEXT_FILE,
        BITMAP_IMAGE
    }

    /**
     * Main entry point of the application.
     *
     * @param args Input arguments.
     * @throws IOException
     */
    public static void main(final String[] args) throws IOException {
        new Main();
    }

    /**
     * Main constructor.
     *
     * @throws IOException
     */
    private Main() throws IOException {
        // Initialize all variables:

        // Data object holder.
        Data dataObject = null;
        // Source of the input for algorithm.
        final DATA_SOURCE dataSource = DATA_SOURCE.TEXT_FILE;

        switch (dataSource) {
            case BITMAP_IMAGE:
                dataObject = getDataFromBitmapFile("input_image.bmp");
                break;
            case TEXT_FILE:
                dataObject = getDataFromTextFile("/home/yurii/dev/MarchingSquaresJava/src/main/resources/data.txt");
                break;
        }

        // Path to input image.
        final String sourceImagePath = "input_image.bmp";
        // Algorithm class.
        final MarchingSquares marchingSquares = new MarchingSquares();
        // All extracted path objects.
        final List<Path> paths = new ArrayList<>();
        // Iterator index.
        int i;
        // Number of attempts of the algorithm usage.
        int n = 1;
        // Start of the time measuring.
        long start;
        // Duration of the algorithm, in milliseconds.
        long duration;
        // Total duration of all iterations, in milliseconds.
        long totalDuration = 0;

        for (i = 0; i < n; i++) {
            // Start time measuring.
            start = System.currentTimeMillis();

            // Initialize algorithm.
            marchingSquares.init(dataObject.imageWidth, dataObject.imageHeight, dataObject.data);
            // Get all known shapes.
            paths.clear();
            paths.addAll(marchingSquares.identifyPerimeter());

            // Get duration.
            duration = (System.currentTimeMillis() - start);
            // Sum all durations.
            totalDuration += duration;

            // Threshold small and inner shapes.
            thresholdPathList(dataObject.imageWidth, dataObject.imageHeight, paths);

            // Print single shape number, must be 26.
            System.out.println(
                    "Found " + paths.size() + " shapes in " + duration + " ms"
            );

            // Draw resulted path objects.
            drawOutputImage(dataObject.imageWidth, dataObject.imageHeight, paths);
        }

        System.out.println("Avg time of process:" + (totalDuration / n) + " ms");
    }

    /**
     * Threshold small and inner shapes.
     *
     * @param imgWidth  Width of the image.
     * @param imgHeight Height of the image.
     * @param paths     List of the Path objects.
     */
    private void thresholdPathList(final int imgWidth,
                                   final int imgHeight,
                                   final List<Path> paths) {
        // Single Path.
        Path path;
        // Inner Path object (this object need to be excluded from collection).
        Path pathInner;
        // Iterator index.
        int i;
        // Iterator index.
        int j;
        // Width threshold to determine small shape (shape to be excluded later).
        int minWidth = imgWidth * 3 / 100;
        // Height threshold to determine small shape (shape to be excluded later).
        int minHeight = imgHeight * 3 / 100;

        // Eliminate small objects
        for (j = 0; j < paths.size(); j++) {
            path = paths.get(j);
            if (path.getShapeWidth() < minWidth || path.getShapeHeight() < minHeight) {
                paths.remove(path);
                j--;
            }
        }

        // Eliminate nested shapes
        for (j = 0; j < paths.size(); j++) {

            path = paths.get(j);
            for (i = j + 1; i < paths.size(); i++) {

                pathInner = paths.get(i);
                if (path.getMinX() <= pathInner.getMinX()
                        && path.getMinY() <= pathInner.getMinY()
                        && path.getMaxX() >= pathInner.getMaxX()
                        && path.getMaxY() >= pathInner.getMaxY()) {

                    paths.remove(pathInner);
                    i--;
                }
            }
        }
    }

    /**
     * Process data text file and constructs appropriate value object holder to pass further to handle by
     * algorithm.
     *
     * @param filePath Path to the data text file.
     * @return Data value object.
     * @throws IOException
     */
    private Data getDataFromTextFile(final String filePath) throws IOException {
        int counter = 0;
        int width = 0;
        int height = 0;
        int[] data = null;
        try (final BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                switch (counter++) {
                    case 0:
                        width = Integer.valueOf(line);
                        break;
                    case 1:
                        height = Integer.valueOf(line);
                        break;
                    default:
                        if (data == null) {
                            data = new int[width * height];
                        }
                        if ("1".equals(line)) {
                            data[counter - 3] = 1;
                        } else {
                            data[counter - 3] = 0;
                        }
                }
                line = bufferedReader.readLine();
            }
        }

        final Data dataObject = new Data();
        dataObject.data = data;
        dataObject.imageWidth = width;
        dataObject.imageHeight = height;

        return dataObject;
    }

    /**
     * Process bitmap image file and constructs appropriate value object holder to pass further to handle by
     * algorithm.
     *
     * @param filePath Path to the bitmap image file.
     * @return Data value object.
     * @throws IOException
     */
    private Data getDataFromBitmapFile(final String filePath) throws IOException {
        // Iterator index.
        int i;
        // Red component value of the single pixel.
        int redComponent;
        // Value of single pixel.
        int pixel;
        // Read input image.
        final BufferedImage image = ImageIO.read(getClass().getResourceAsStream(filePath));
        // Image width.
        final int imgWidth = image.getWidth();
        // Image height.
        final int imgHeight = image.getHeight();
        // Data (array of pixels) associated with the given image.
        final int[] pixels = new int[imgWidth * imgHeight];
        // get all pixels of the image.
        image.getRGB(0, 0, imgWidth, imgHeight, pixels, 0, imgWidth);

        // Assign 1 to red color and z to all other pixels.
        for (i = 0; i < pixels.length; i++) {
            pixel = pixels[i];
            redComponent = (pixel >> 16) & 0xff;
            if (redComponent != 0) {
                pixels[i] = 1;
            } else {
                pixels[i] = 0;
            }
        }

        final Data dataObject = new Data();
        dataObject.data = pixels;
        dataObject.imageWidth = imgWidth;
        dataObject.imageHeight = imgHeight;

        return dataObject;
    }

    /**
     * Draw provided path object to visualize data.
     *
     * @param width  Width of the original input image.
     * @param height Height of the original input image.
     * @param paths  List of the path objects.
     * @throws IOException
     */
    private void drawOutputImage(int width, int height, final List<Path> paths) throws IOException {
        Direction direction;
        int lastX;
        int lastY;
        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = image.createGraphics();
        graphics2D.setColor(Color.BLUE);

        for (final Path path : paths) {

            lastX = path.getOriginX();
            lastY = -path.getOriginY();

            for (int i = 0; i < path.getLength(); i++) {

                direction = path.getDirections()[i];

                lastX += direction.screenX;
                lastY += direction.screenY;

                graphics2D.drawRect(lastX, lastY, 1, 1);
            }
        }

        final File outputFile = new File("src/main/resources/com/yurii/marchingsquares/output_image.bmp");
        ImageIO.write(image, "bmp", outputFile);
    }

    /**
     * Value object for the input data.
     */
    private static final class Data {

        /**
         * Array of the binary pixels (1 or 0 value).
         */
        private int[] data;
        /**
         * Input image width.
         */
        private int imageWidth;
        /**
         * Input image height.
         */
        private int imageHeight;

        private Data() {
            super();
        }
    }
}
